var CloudFileSystem = function(callbacks){
	this.callbacks = {};
	callbacks = callbacks ? callbacks : {};
	for( var i in CloudFileSystem.prototype.callbacks ){
		this.callbacks[i] = typeof callbacks[i] == 'function' ? callbacks[i] : CloudFileSystem.prototype.callbacks[i];
	}
}
CloudFileSystem.prototype.callbacks = {
	list_files: function(){},
	get_file: function(){},
	put_file: function(){},
	delete_file: function(){}
};
CloudFileSystem.prototype.validate_folder_path = function(path){
	path = path.match(/^(\/.*)?\/$/);
	if( path == null ){
		throw new Error('"' + path + '" is not a valid folder name');
	}else{
		return path[0];
	}
}
CloudFileSystem.prototype.validate_file_path = function(path){
	path = path.match(/^\/.*[^\/]$/);
	if( path == null ){
		throw new Error('"' + path + '" is not a valid file name');
	}else{
		return path[0];
	}
}
CloudFileSystem.prototype.split_path = function(path){
	return path.split('/');
}
CloudFileSystem.prototype.basename = function(path){
	var files = this.split_path(this.validate_file_path(path));
	return files.length > 0 ? files.pop() : '';
}
CloudFileSystem.prototype.dirname = function(path){
	if(path == '/') return '/';
	var files = this.split_path(this.validate_file_path(path));
	if( files.length > 0 ){
		files[files.length-1] = '';
	}
	return files.join('/')
}
CloudFileSystem.prototype.list_files = function(path){
	var callback = this.callbacks;
	path = this.validate_folder_path(path);
	$.ajax({
		type: 'GET',
		url: path,
		success: function(data){
			if( callback.list_files ) callback.list_files(data.objects, data.path, data.parent);
		}
	});
}
CloudFileSystem.prototype.get_file = function(path){
	var callback = this.callbacks;
	path = this.validate_file_path(path);
	$.ajax({
		type: 'GET',
		url: path,
		success: function(message){
			if( callback.get_file ) callback.get_file(message);
		}
	});
}
CloudFileSystem.prototype.put_file = function(path, data){
	var callback = this.callbacks;
	path = this.validate_file_path(path);
	$.ajax({
		type: 'PUT',
		url: path,
		data: data,
		success: function(message){
			if( callback.put_file ) callback.put_file(message);
		}
	});
}
CloudFileSystem.prototype.copy_file = function(src, dest){
	var callback = this.callbacks;
	dest = this.validate_file_path(dest);
	src = this.validate_file_path(src);
	$.ajax({
		type: 'PUT',
		url: dest,
		headers: {
			'copy-source': src
		},
		success: function(message){
			if( callback.copy_file ) callback.copy_file(message);
		}
	});
}
CloudFileSystem.prototype.delete_file = function(path){
	var callback = this.callbacks;
	path = this.validate_file_path(path);
	$.ajax({
		type: 'DELETE',
		url: path,
		success: function(message){
			if( callback.delete_file ) callback.delete_file(message);
		}
	});
}
