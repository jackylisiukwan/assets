var Browser = {
	settings: {
		parent: '/'
	},
	uploaded_id: 0,
	init: function(){
		$('#toolbar .view').click(function(e){
			e.preventDefault();
			$('#files-list')
				.removeClass($('#files-list').attr('class'))
				.addClass($(this).attr('href').match(/^#(.*)/)[1]);
		});
		$('a[href="#upper"]').click(function(e){
			e.preventDefault();
			Browser.list_files(Browser.settings.parent);
		});
		$('#address').parent('form').submit(function(e){
			e.preventDefault();
			Browser.list_files($('#address', this).val());
		});
		Browser.list_files('/');
	},
	humanized_size: function(size){
		size = parseInt(size);
		var base = 1024;
		var exp = size == 0 ? 0 : Math.floor(Math.log(size) / Math.log(base));
		var norm_size = Math.round(size / Math.pow(base, exp) * 100) / 100;
		return (norm_size + ' ' + ['byte', 'KB', 'MB', 'GB'][exp]) + ' ';
	},
	list_files: function(path){
		$.ajax($.extend({
			type: 'GET',
			url: path,
			dataType: 'json',
			success: function(data){
				var $files_list = $('#files-list');
				var $container = $('<div></div>');
				$files_list.empty();
				$files_list.append($container);
				var $address = $('#address');
				$address.val(data.path);
				Browser.settings.parent = data.parent;
				$.each(data.objects, function(index, file){
					var div_html = '<div></div>';
					var $item = $('<a></a>');
					var $name = $(div_html);
					var fullpath = file.path;
					$name.addClass('name');
					$name.text(file.name);
					$item.attr('href', '#' + fullpath);
					if( file.type == 'file' ){
						$item.addClass('file');
						var header = $.param({
							'response-content-disposition': 'attachment; filename=' + file.name,
							'response-content-type': 'application/octet-stream',
						});
						/*
						$name.css({
							'background-image': 'url(' + file.path + ')',
							'background-size': 'contain'
						});
						*/
						$item.click(function(e){
							e.preventDefault();
						}).dblclick(function(e){
							e.preventDefault();
							document.location = (encodeURI(fullpath) + '?' + header);
						});
					}else{
						$item.addClass('folder');
						$item.dblclick(function(e){
							Browser.list_files(fullpath);
						});
					}
					$container.append($item
															.append($name)
															.append($(div_html).addClass('size').text(file.size))
															.append($(div_html).addClass('atime').text(new Date(file.atime * 1000).toDateString()))
															.append($(div_html).addClass('mtime').text(new Date(file.mtime * 1000).toDateString()))
															.append($(div_html).addClass('ctime').text(new Date(file.ctime * 1000).toDateString()))
															.append($(div_html).addClass('delete').click(function(){
																if( confirm("Are you sure to remove " + fullpath + "?") ) $.ajax({
																	type: 'DELETE',
																	url: fullpath,
																	success: function(){
																		Browser.list_files(path);
																	}
																});
															})));
					
				});
				$container.addClass('upload-panel');
				$container.fileUploadUI({
					url: path,
					namespace: 'file_upload_' + Browser.uploaded_id++,
					fieldName: 'file',
					method: 'POST',
					uploadTable: $('#upload_files'),
					downloadTable: $('#download_files'),
					onProgress: function(e, files){
						var $progress = $('.progress');
						$progress.each(function(){
							$progress.show();
							$progress.children('span.bar').width(e.loaded * 100 / e.totalSize + '%');
							$progress.children('span.description').val(files[0]);
						});
					},
					onComplete: function(e, files, n, xhr, hld){
						$('#progress').hide();
						Browser.list_files(hld.url);
					}
				});
				$files_list.find('.size').each(function(index, element){
					$(element).text(Browser.humanized_size($(element).text()));
				});
			}
		}, typeof options == 'undefined' ? {} : options));
	}
};
