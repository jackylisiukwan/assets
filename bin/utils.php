<?php
function uri_path(){
	$args = func_get_args();
	return implode(URI_PATH_SEPARATOR, $args);
}
function file_path(){
	$args = func_get_args();
	return implode(FILE_PATH_SEPARATOR, $args);
}
function is_child_of($path, $parent_path){
	$path = explode(FILE_PATH_SEPARATOR, $path);
	$parent_path = explode(FILE_PATH_SEPARATOR, $parent_path);
	#print_r($path);
	#print_r($parent_path);
	#print_r($parent_path + $path);
	return ($parent_path + $path) === $path;
}

function to_dir_uri_path($path){
	$path = to_file_uri_path($path);
	return $path === '/' ? $path : ($path.'/');
}

function to_file_uri_path($path){
	return $path = str_replace("\\", '/', substr($path, strlen(FILES_ROOT)));
}

function to_uri_path($path){
	return is_dir($path) ? to_dir_uri_path($path) : to_file_uri_path($path);
}

function list_files($realpath){
	$dh = opendir($realpath);
	echo '[';
	$files_info = array();
	while(($file = readdir($dh)) !== false){
		if( $file === '.' || $file === '..') continue;
		$files_info[] = json_encode(file_info($realpath, $file));
	}
	closedir($dh);
	echo implode(',', $files_info);
	echo ']';
}

function read_file($realpath){
	$headers = array(
		'response-content-type',
		'response-content-language',
		'response-expires',
		'response-cache-control',
		'response-content-disposition',
		'response-content-encoding'
	);
	$start = strlen('response-');
	foreach( $headers as $header ){
		if( array_key_exists($header, $_REQUEST) ){
			$formated_header = strtoupper(substr($header, $start, 1)).substr($header, $start+1);
			header("$formated_header: {$_REQUEST[$header]}");
		}
	}
	readfile($realpath);
}

function file_info($path, $filename){
	$fullpath = file_path($path, $filename);
	$stat = stat($fullpath);
	$info = array();
	$url_path = to_uri_path(realpath($fullpath));
	$info['name'] = filename_encode($filename);
	$info['path'] = filename_encode($url_path);
	$info['type']  = is_dir($fullpath) ? 'directory' : 'file';
	$info['size']  = $stat['size'];
	$info['mode']  = $stat['mode'];
	$info['atime'] = $stat['atime'];
	$info['mtime'] = $stat['mtime'];
	$info['ctime'] = $stat['ctime'];
	return $info;
}

function filename_encode($filename){
	return iconv(mb_detect_encoding($filename), 'utf-8', $filename);
}

function check_accessible($path){
	$uri_path = uri_path(FILES_ROOT, $path);
	$dirname = $uri_path;
	$last_dirname = null;
	do{
		if( $realpath = realpath($dirname) ){
			return ( is_child_of($realpath, FILES_ROOT) );
		}
		$pathinfo = pathinfo($dirname);
		$last_dirname = $dirname;
		$dirname = $pathinfo['dirname'];
		$basename = $pathinfo['basename'];
	}while($last_dirname != $dirname);
	return false;
}

function get_realpath($path){
	$realpath = realpath(file_path(FILES_ROOT, $path));
	if( is_child_of($realpath, FILES_ROOT) ){
		return $realpath;
	}else{
		return false;
	}
}

function check_existing($path){
	return !!realpath(file_path(FILES_ROOT, $path));
}

function __test_utils__(){
	echo '<pre>';
	$paths = array(
		'.',
		'utils',
		'../bin',
		'not/existing',
		'../not/existing'
	);

	foreach( $paths as $path ){
		echo '"'.$path.'" ('.get_realpath($path).'):';
		echo "\n";
		echo '- '.(check_existing($path) ? 'existing' : 'NOT existing');
		echo "\n";
		echo '- '.(check_accessible($path) ? 'accessible' : 'NOT accessible');
		echo "\n";
	}
	echo '</pre>';
}
?>