<?php
define(APPLICATION_ROOT, realpath('..'));
define(FILES_ROOT, realpath('../files'));
define(FILE_PATH_SEPARATOR, DIRECTORY_SEPARATOR);
define(URI_PATH_SEPARATOR, '/');

include 'errors.php';
include 'utils.php';
include 'dispatch.php';
include 'plugin.php';

dispatch();

function doGet($path){
	if( $realpath = get_realpath($path) ){
		header('HTTP/1.1 200 OK');
		if( is_dir($realpath) ){
			header('Content-type: application/json');
			header('Content-Encoding: utf-8');
			echo '{';
			echo '"path":', json_encode(to_dir_uri_path($realpath)), ',';
			echo '"parent":', json_encode(to_dir_uri_path(dirname($realpath))), ',';
			echo '"objects":';
			list_files($realpath);
			echo '}';
		}else{
			$filesize = filesize($realpath);
			header("Content-length: $filesize");
			read_file(__plugin_apply_filter__('read_file', $realpath));
		}
	}else{
		header('HTTP/1.1 404 Not Found');
	}
}
function doPost($path){
	if( $_FILES['file'] && $_FILES['file']['error'] == null ){
		if( check_accessible($path) ){
			$file_path = file_path(FILES_ROOT, $path);
			@mkdir($file_path, fileperms(FILES_ROOT), true);
			$realpath = file_path($file_path, $_FILES['file']['name']);
			if(@move_uploaded_file($_FILES['file']['tmp_name'], $realpath)){
				header('HTTP/1.1 200 OK');
				echo json_encode(array(
					url => uri_path($path, $_FILES['file']['name'])
				));
			}else{
				header('HTTP/1.1 500 OK');
				echo json_encode(array(
					path => $path,
					error => error_get_last()
				));
			}
		}else{
			header('HTTP/1.1 500 Server error');
			echo json_encode(array(
				error => $_FILES['file']['error']
			));
		}
	}else{
		header('HTTP/1.1 403 Forbidden');
		echo json_encode(array(
			error => error_get_last()
		));
	}
}
function doPut($path){
	if( check_accessible($path) ){
		$file_path = file_path(FILES_ROOT, $path);
		@mkdir(dirname($file_path), fileperms(FILES_ROOT), true);
		$realpath = file_path($file_path);
		$realpath = str_replace('/', "\\", $realpath);
		$realpath = str_replace('\\\\', "\\", $realpath);
		$headers = getallheaders();
		if( isset($headers['copy-source']) ){
			$copy_source = $headers['copy-source'];
			$is = check_existing($copy_source) && check_accessible($copy_source) ? @fopen(file_path(FILES_ROOT, $copy_source), 'r') : false;
			if($is === false){
				header('HTTP/1.1 403 Forbidden');
				echo json_encode(array(
					error => error_get_last()
				));
				return;
			}
		}else{
			$is = @fopen('php://input', 'r');
			if($is === false){
				header('HTTP/1.1 500 Server error');
				echo json_encode(array(
					path => $path,
					error => $error
				));
				return;
			}
		}
		if($os = @fopen($realpath, 'w')){
			$contents = array();
			while( !feof($is) && ( $buffer = fread($is, 1024)) !== false ){
				if( fwrite($os, $buffer) === false ){
					return;
				}
			}
			header('HTTP/1.1 200 OK');
			if( $headers['copy-source'] ){
				echo json_encode(array(
					url => uri_path($path),
					size => filesize($realpath)
				));
			}else{
				echo json_encode(array(
					url => uri_path($path),
					size => filesize($realpath)
				));
			}
			fclose($os);
		}else{
			$error = error_get_last();
			header('HTTP/1.1 500 Server error');
			echo json_encode(array(
				path => $path,
				error => $error
			));
		}
	}else{
		header('HTTP/1.1 403 Forbidden');
		echo json_encode(array(
			error => error_get_last()
		));
	}
}
function doDelete($path){
	if( check_existing($path) && check_accessible($path) ){
		unlink(get_realpath($path));
	}else{
		header('HTTP/1.1 404 Not found');
	}
}

?>