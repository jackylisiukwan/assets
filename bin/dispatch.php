<?php
function dispatch(){
	if( $GLOBALS['test'] == true ) echo "This is a {$_SERVER['REQUEST_METHOD']}\n";
	$uri_info = parse_url($_SERVER['REQUEST_URI']);
	$path = urldecode($uri_info['path']);
	$method = $_SERVER['REQUEST_METHOD'];
	switch($method){
		case 'GET': 	  doGet($path);    break;
		case 'POST': 	  doPost($path);   break;
		case 'PUT':     doPut($path);    break;
		case 'DELETE': 	doDelete($path); break;
	}
}

function __test_dispatch__(){
	$GLOBALS['test'] = true;
	dispatch();
	?>
<!DOCTYPE html>
<html>
<style>
form{
	display: inline;
}
</style>
<body>
<pre>
GET: <a href="./test.php">Go!</a>
POST: <form action="./test.php" method="POST"><input type="submit" value="Go!"></form>
PUT: <a href="./test.php" data-method="delete">Go!</a>
DELETE: <a href="./test.php" data-method="delete">Go!</a>
</pre>
</body>
</html>
	<?php
}
?>