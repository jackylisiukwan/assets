<?
$filters = array(
	read_file => array()
);

function __plugin_apply_filter__($filter, $params){
	global $filters;
	$filter_hooks = $filters[$filter];
	foreach($filter_hooks as $function){
		call_user_func($function, &$params);
	}
	return $params;
}

function has_filter($filter, $function){
	global $filters;
	if( array_key_exists($filter, $filters) ){
		$filter_hooks = &$filters[$filter];
		return in_array($function, $filter_hooks);
	}
}

function add_filter($filter, $function){
	global $filters;
	if( array_key_exists($filter, $filters) ){
		$filter_hooks = &$filters[$filter];
		$filter_hooks[] = $function;
	}
}

$plugins_path = APPLICATION_ROOT.'/plugins';
$plugins_dh = opendir($plugins_path);
while(($file = readdir($plugins_dh)) !== false){
	if( $file === '.' || $file === '..' || !is_dir($plugin_path = realpath("$plugins_path/$file"))) continue;
	$plugin_dh = opendir($plugin_path);
	while(($file = readdir($plugin_dh)) !== false){
		if( $file === '.' || $file === '..' || is_dir($hook_path = realpath("$plugin_path/$file"))) continue;
		require_once($hook_path);
	}
	closedir($plugin_dh);
}
closedir($plugins_dh);
?>